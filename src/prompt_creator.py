from sentence_transformers import SentenceTransformer, util
from src import Similarities


class PromptCreator:
    """
    Creates a prompt for a given sentence with the topmost similar sentences
    """
    def __init__(self, prompt_data, chkpt='sentence-transformers/all-MiniLM-L6-v2', top=5):
        """
        :param prompt_sentences: List of sentences to select for prompt
        :param chkpt: Name of the SentenceTransformer model for embeddings
        :param top: # of nearest sentences to include in prompt
        """
        self.model = SentenceTransformer(chkpt)
        self.top = top
        self.prompt_sentences = [prompt_data[i][0] for i in range(len(prompt_data))]
        self.prompt_graphs = [prompt_data[i][1] for i in range(len(prompt_data))]
        self.similarities = Similarities(self.prompt_sentences)  # Should return indexed values

    def create_prompt(self, sentence):
        """
        Create prompt to use in LLM
        :param sentence:str input to create a prompt for
        :return: prompt:str
        """
        header = ";;; Translate sentences into Lisp expressions\n\n"
        sent_marker = ';sentence: '
        graph_marker = 'AMR: '
        prompt = header
        sims = self.similarities.calculate(sentence)
        for i in range(self.top):
            sent_id = sims[i][1]  # Take index of next similar sent
            prompt_sentence = sent_marker + self.prompt_sentences[sent_id] + "\n"  # Append next similar sent
            prompt_graph = graph_marker + self.prompt_graphs[sent_id] + "\n"  # Append graph for next similar sent
            prompt += prompt_sentence + prompt_graph + "\n"
        query = sent_marker + sentence
        prompt += query
        return prompt
