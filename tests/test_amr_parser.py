import io
import unittest
from src import AMRParser


class TestAMRParser(unittest.TestCase):
    def test_AMRParser(self):
        data = AMRParser('test_sentences.txt').parse()
        self.assertEqual(data[0], ['This is a sentence', '# ::snt This is a sentence\n(s / sentence\n      :domain (t / this))'])
        self.assertEqual(data[1], ['This is another one', '# ::snt This is another one\n(o / one\n      :mod (a / another)\n      :domain (t / this))'])
        self.assertEqual(data[2], ['One more', '# ::snt One more\n(o / one\n      :mod (m / more))'])


if __name__ == '__main__':
    unittest.main()
