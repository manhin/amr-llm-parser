import unittest

from src import AMRData


class TestAMRData(unittest.TestCase):
    def test_parse_data(self):
        data = AMRData("test_data.txt")
        expected_samples = [("The girl adjusted the machine.", "(a / adjust-01 :ARG0 (b / girl) :ARG1 (m / machine))"),
                            ("The machine was adjusted by the girl.", "(a / adjust-01 :ARG0 (b / girl) :ARG1 (m / machine))"),
                            ("The killing happened yesterday.", "(k / kill-01 :time (y / yesterday))"),
                            ("The killing took place yesterday.", "(k / kill-01 :time (y / yesterday))"),
                            ("The boy doesn’t know the girl came.", "(k / know-01 :polarity - :ARG0 (b / boy) :ARG1 (c / come-01 :ARG1 (g / girl)))"),
                            ("Do you want tea or coffee?", "(w / want-01 :ARG0 (y / you) :ARG1 (a / amr-choice :op1 (t / tea) :op2 (c / coffee)))")]
        self.assertEqual(data.samples, expected_samples)

    def test_split_data(self):
        data = AMRData('test_data2.txt')
        training_data, evaluation_data = data.split_data(0.1)

        # check that the correct number of samples are in each set
        self.assertEqual(len(training_data) + len(evaluation_data), len(data.samples))


if __name__ == '__main__':
    unittest.main()
