;sentence: the marble that is white
AMR: (m / marble :ARG1-of (w / white-03))

;sentence: The boy sees that the marble is white.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (w / white-03 :ARG1 (m / marble)))

;sentence: The boy sees the whiteness of the marble.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (w / white-03 :ARG1 (m / marble)))

;sentence: The boy sees the white marble.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (m / marble :ARG1-of (w / white-03)))

;sentence: The boy sees the marble that is white.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (m / marble :ARG1-of (w / white-03)))

;sentence: The boy saw the girl who wanted him.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (g / girl :ARG0-of (w / want-01 :ARG1 b)))

;sentence: The boy saw the girl who he was wanted by.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (g / girl :ARG0-of (w / want-01 :ARG1 b)))

;sentence: The girl who wanted the boy was seen by him.
AMR: (s / see-01 :ARG0 (b / boy) :ARG1 (g / girl :ARG0-of (w / want-01 :ARG1 b)))

;sentence: The girl who was seen by the boy wants him.
AMR: (w / want-01 :ARG0 (g / girl :ARG1-of (s / see-01 :ARG0 (b / boy))) :ARG1 b)

;sentence: The boy is wanted by the girl he saw.
AMR: (w / want-01 :ARG0 (g / girl :ARG1-of (s / see-01 :ARG0 (b / boy))) :ARG1 b)

;sentence: The regulatory documents were changed.
AMR: (c / change-01 :ARG1 (d / document :instrument-of (r / regulate-01)))

;sentence: The boy wants to believe.
AMR: (w / want-01 :ARG0 (b / boy) :ARG1 (b2 / believe-01 :ARG0 b))

;sentence: The boy believes.
AMR: (b / believe-01 :ARG0 (b2 / boy))

;sentence: He described the mission as a failure.
AMR: (d / describe-01 :ARG0 (h / he) :ARG1 (m / mission) :ARG2 (f / fail-01))

;sentence: As he described it, the mission was a failure.
AMR: (d / describe-01 :ARG0 (h / he) :ARG1 (m / mission) :ARG2 (f / fail-01))

;sentence: His description of the mission: failure.
AMR: (d / describe-01 :ARG0 (h / he) :ARG1 (m / mission) :ARG2 (f / fail-01))

;sentence: a band of marauders
AMR: (b / band :consist-of (p / person :ARG0-of (m / maraud-00)))

;sentence: The boy can go.
AMR: (p / possible-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: It is possible that the boy goes.
AMR: (p / possible-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy must go.
AMR: (o / obligate-01 :ARG2 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy is obligated to go.
AMR: (o / obligate-01 :ARG2 (g / go-02 :ARG0 (b / boy)))

;sentence: It is obligatory that the boy go.
AMR: (o / obligate-01 :ARG2 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy may go.
AMR: (o / permit-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy is permitted to go.
AMR: (o / permit-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: It is permissible that the boy go.
AMR: (o / permit-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: It may rain.
AMR: (p / possible-01 :ARG1 (r / rain-01))

;sentence: It might rain.
AMR: (p / possible-01 :ARG1 (r / rain-01))

;sentence: Rain is possible.
AMR: (p / possible-01 :ARG1 (r / rain-01))

;sentence: It’s possible that it will rain.
AMR: (p / possible-01 :ARG1 (r / rain-01))

;sentence: The boy should go.
AMR: (r / recommend-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: It is recommended that the boy go.
AMR: (r / recommend-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy is likely to go.
AMR: (l / likely-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: It is likely that the boy will go.
AMR: (l / likely-01 :ARG1 (g / go-02 :ARG0 (b / boy)))

;sentence: The boy would rather go.
AMR: (p / prefer-01 :ARG0 (b / boy) :ARG1 (g / go-02 :ARG0 b))

;sentence: The boy prefers to go.
AMR: (p / prefer-01 :ARG0 (b / boy) :ARG1 (g / go-02 :ARG0 b))

;sentence: I am used to working.
AMR: (u / use-02 :ARG0 (i / i) :ARG1 (w / work-01 :ARG0 i))

;sentence: The boy doesn’t go.
AMR: (g / go-02 :ARG0 (b / boy) :polarity -)
